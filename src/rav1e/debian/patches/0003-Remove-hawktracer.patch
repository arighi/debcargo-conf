From: Sebastian Ramacher <sramacher@debian.org>
Date: Thu, 29 Dec 2022 11:14:32 +0100
Subject: Remove hawktracer

---
 Cargo.toml             | 4 ----
 src/activity.rs        | 3 ---
 src/api/internal.rs    | 9 ---------
 src/api/lookahead.rs   | 5 -----
 src/bin/muxer/ivf.rs   | 2 --
 src/bin/stats.rs       | 3 ---
 src/cdef.rs            | 2 --
 src/deblock.rs         | 4 ----
 src/encoder.rs         | 3 ---
 src/lrf.rs             | 2 --
 src/me.rs              | 3 ---
 src/scenechange/mod.rs | 5 -----
 12 files changed, 45 deletions(-)

diff --git a/Cargo.toml b/Cargo.toml
index 37112c9..a1ff12d 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -135,9 +135,6 @@ version = "1.0"
 [dependencies.rayon]
 version = "1.0"
 
-[dependencies.rust_hawktracer]
-version = "0.7.0"
-
 [dependencies.scan_fmt]
 version = "0.2.3"
 optional = true
@@ -219,7 +216,6 @@ quick_test = []
 scenechange = []
 serialize = ["serde", "toml", "v_frame/serialize", "arrayvec/serde"]
 signal_support = ["signal-hook"]
-tracing = ["rust_hawktracer/profiling_enabled"]
 unstable = []
 wasm = ["wasm-bindgen"]
 [target."cfg(any(decode_test, decode_test_dav1d))".dependencies.system-deps]
diff --git a/src/activity.rs b/src/activity.rs
index 99d9137..7ebcd0e 100644
--- a/src/activity.rs
+++ b/src/activity.rs
@@ -12,7 +12,6 @@ use crate::rdo::{ssim_boost, DistortionScale};
 use crate::tiling::*;
 use crate::util::*;
 use itertools::izip;
-use rust_hawktracer::*;
 
 #[derive(Debug, Default, Clone)]
 pub struct ActivityMask {
@@ -20,7 +19,6 @@ pub struct ActivityMask {
 }
 
 impl ActivityMask {
-  #[hawktracer(activity_mask_from_plane)]
   pub fn from_plane<T: Pixel>(luma_plane: &Plane<T>) -> ActivityMask {
     let PlaneConfig { width, height, .. } = luma_plane.cfg;
 
@@ -55,7 +53,6 @@ impl ActivityMask {
     ActivityMask { variances: variances.into_boxed_slice() }
   }
 
-  #[hawktracer(activity_mask_fill_scales)]
   pub fn fill_scales(
     &self, bit_depth: usize, activity_scales: &mut Box<[DistortionScale]>,
   ) {
diff --git a/src/api/internal.rs b/src/api/internal.rs
index c1613c8..5bfa3ab 100644
--- a/src/api/internal.rs
+++ b/src/api/internal.rs
@@ -27,7 +27,6 @@ use crate::stats::EncoderStats;
 use crate::tiling::Area;
 use crate::util::Pixel;
 use arrayvec::ArrayVec;
-use rust_hawktracer::*;
 use std::cmp;
 use std::collections::{BTreeMap, BTreeSet};
 use std::env;
@@ -312,7 +311,6 @@ impl<T: Pixel> ContextInner<T> {
     }
   }
 
-  #[hawktracer(send_frame)]
   pub fn send_frame(
     &mut self, mut frame: Option<Arc<Frame<T>>>,
     params: Option<FrameParameters>,
@@ -591,7 +589,6 @@ impl<T: Pixel> ContextInner<T> {
   /// `rec_buffer` and `lookahead_rec_buffer` on the `FrameInvariants`. This
   /// function must be called after every new `FrameInvariants` is initially
   /// computed.
-  #[hawktracer(compute_lookahead_motion_vectors)]
   fn compute_lookahead_motion_vectors(&mut self, output_frameno: u64) {
     let qps = {
       let frame_data = self.frame_data.get(&output_frameno).unwrap();
@@ -752,7 +749,6 @@ impl<T: Pixel> ContextInner<T> {
 
   /// Computes lookahead intra cost approximations and fills in
   /// `lookahead_intra_costs` on the `FrameInvariants`.
-  #[hawktracer(compute_lookahead_intra_costs)]
   fn compute_lookahead_intra_costs(&mut self, output_frameno: u64) {
     let frame_data = self.frame_data.get(&output_frameno).unwrap();
     let fi = &frame_data.fi;
@@ -774,7 +770,6 @@ impl<T: Pixel> ContextInner<T> {
     );
   }
 
-  #[hawktracer(compute_keyframe_placement)]
   pub fn compute_keyframe_placement(
     &mut self, lookahead_frames: &[Arc<Frame<T>>],
   ) {
@@ -791,7 +786,6 @@ impl<T: Pixel> ContextInner<T> {
     self.next_lookahead_frame += 1;
   }
 
-  #[hawktracer(compute_frame_invariants)]
   pub fn compute_frame_invariants(&mut self) {
     while self.set_frame_properties(self.next_lookahead_output_frameno).is_ok()
     {
@@ -804,7 +798,6 @@ impl<T: Pixel> ContextInner<T> {
     }
   }
 
-  #[hawktracer(update_block_importances)]
   fn update_block_importances(
     fi: &FrameInvariants<T>, me_stats: &crate::me::FrameMEStats,
     frame: &Frame<T>, reference_frame: &Frame<T>, bit_depth: usize,
@@ -963,7 +956,6 @@ impl<T: Pixel> ContextInner<T> {
   }
 
   /// Computes the block importances for the current output frame.
-  #[hawktracer(compute_block_importances)]
   fn compute_block_importances(&mut self) {
     // SEF don't need block importances.
     if self.frame_data[&self.output_frameno].fi.show_existing_frame {
@@ -1296,7 +1288,6 @@ impl<T: Pixel> ContextInner<T> {
     }
   }
 
-  #[hawktracer(receive_packet)]
   pub fn receive_packet(&mut self) -> Result<Packet<T>, EncoderStatus> {
     if self.done_processing() {
       return Err(EncoderStatus::LimitReached);
diff --git a/src/api/lookahead.rs b/src/api/lookahead.rs
index 690acd2..30297d3 100644
--- a/src/api/lookahead.rs
+++ b/src/api/lookahead.rs
@@ -14,7 +14,6 @@ use crate::rayon::iter::*;
 use crate::tiling::{Area, TileRect};
 use crate::transform::TxSize;
 use crate::{Frame, Pixel};
-use rust_hawktracer::*;
 use std::sync::Arc;
 use v_frame::pixel::CastFromPrimitive;
 
@@ -24,7 +23,6 @@ pub(crate) const IMP_BLOCK_SIZE_IN_MV_UNITS: i64 =
 pub(crate) const IMP_BLOCK_AREA_IN_MV_UNITS: i64 =
   IMP_BLOCK_SIZE_IN_MV_UNITS * IMP_BLOCK_SIZE_IN_MV_UNITS;
 
-#[hawktracer(estimate_intra_costs)]
 pub(crate) fn estimate_intra_costs<T: Pixel>(
   frame: &Frame<T>, bit_depth: usize, cpu_feature_level: CpuFeatureLevel,
 ) -> Box<[u32]> {
@@ -116,7 +114,6 @@ pub(crate) fn estimate_intra_costs<T: Pixel>(
   intra_costs.into_boxed_slice()
 }
 
-#[hawktracer(estimate_importance_block_difference)]
 pub(crate) fn estimate_importance_block_difference<T: Pixel>(
   frame: Arc<Frame<T>>, ref_frame: Arc<Frame<T>>,
 ) -> Box<[u32]> {
@@ -174,7 +171,6 @@ pub(crate) fn estimate_importance_block_difference<T: Pixel>(
   inter_costs.into_boxed_slice()
 }
 
-#[hawktracer(estimate_inter_costs)]
 pub(crate) fn estimate_inter_costs<T: Pixel>(
   frame: Arc<Frame<T>>, ref_frame: Arc<Frame<T>>, bit_depth: usize,
   mut config: EncoderConfig, sequence: Arc<Sequence>,
@@ -236,7 +232,6 @@ pub(crate) fn estimate_inter_costs<T: Pixel>(
   inter_costs.into_boxed_slice()
 }
 
-#[hawktracer(compute_motion_vectors)]
 pub(crate) fn compute_motion_vectors<T: Pixel>(
   fi: &mut FrameInvariants<T>, fs: &mut FrameState<T>, inter_cfg: &InterConfig,
 ) {
diff --git a/src/bin/muxer/ivf.rs b/src/bin/muxer/ivf.rs
index d89e541..2157a58 100644
--- a/src/bin/muxer/ivf.rs
+++ b/src/bin/muxer/ivf.rs
@@ -12,7 +12,6 @@ use super::Muxer;
 use crate::error::*;
 use ivf::*;
 use rav1e::prelude::*;
-use rust_hawktracer::*;
 use std::fs;
 use std::fs::File;
 use std::io;
@@ -37,7 +36,6 @@ impl Muxer for IvfMuxer {
     );
   }
 
-  #[hawktracer(write_frame)]
   fn write_frame(&mut self, pts: u64, data: &[u8], _frame_type: FrameType) {
     write_ivf_frame(&mut self.output, pts, data);
   }
diff --git a/src/bin/stats.rs b/src/bin/stats.rs
index d363d0c..09e3b77 100644
--- a/src/bin/stats.rs
+++ b/src/bin/stats.rs
@@ -12,7 +12,6 @@ use rav1e::data::EncoderStats;
 use rav1e::prelude::Rational;
 use rav1e::prelude::*;
 use rav1e::{Packet, Pixel};
-use rust_hawktracer::*;
 use std::fmt;
 use std::time::Instant;
 
@@ -30,7 +29,6 @@ pub struct FrameSummary {
   pub enc_stats: EncoderStats,
 }
 
-#[hawktracer(build_frame_summary)]
 pub fn build_frame_summary<T: Pixel>(
   packets: Packet<T>, bit_depth: usize, chroma_sampling: ChromaSampling,
   metrics_cli: MetricsEnabled,
@@ -742,7 +740,6 @@ pub enum MetricsEnabled {
   All,
 }
 
-#[hawktracer(calculate_frame_metrics)]
 pub fn calculate_frame_metrics<T: Pixel>(
   frame1: &Frame<T>, frame2: &Frame<T>, bit_depth: usize, cs: ChromaSampling,
   metrics: MetricsEnabled,
diff --git a/src/cdef.rs b/src/cdef.rs
index 6eeddb0..26e8ae1 100644
--- a/src/cdef.rs
+++ b/src/cdef.rs
@@ -13,7 +13,6 @@ use crate::encoder::FrameInvariants;
 use crate::frame::*;
 use crate::tiling::*;
 use crate::util::{clamp, msb, CastFromPrimitive, Pixel};
-use rust_hawktracer::*;
 
 use crate::cpu_features::CpuFeatureLevel;
 use std::cmp;
@@ -586,7 +585,6 @@ pub fn cdef_filter_superblock<T: Pixel>(
 //   tile boundary), the filtering process ignores input pixels that
 //   don't exist.
 
-#[hawktracer(cdef_filter_tile)]
 pub fn cdef_filter_tile<T: Pixel>(
   fi: &FrameInvariants<T>, input: &Frame<T>, tb: &TileBlocks,
   output: &mut TileMut<'_, T>,
diff --git a/src/deblock.rs b/src/deblock.rs
index ff70368..61b7cd4 100644
--- a/src/deblock.rs
+++ b/src/deblock.rs
@@ -17,7 +17,6 @@ use crate::quantize::*;
 use crate::tiling::*;
 use crate::util::{clamp, ILog, Pixel};
 use crate::DeblockState;
-use rust_hawktracer::*;
 use std::cmp;
 
 use crate::rayon::iter::*;
@@ -1313,7 +1312,6 @@ fn sse_h_edge<T: Pixel>(
 }
 
 // Deblocks all edges, vertical and horizontal, in a single plane
-#[hawktracer(deblock_plane)]
 pub fn deblock_plane<T: Pixel>(
   deblock: &DeblockState, p: &mut PlaneRegionMut<T>, pli: usize,
   blocks: &TileBlocks, crop_w: usize, crop_h: usize, bd: usize,
@@ -1563,7 +1561,6 @@ fn sse_plane<T: Pixel>(
 }
 
 // Deblocks all edges in all planes of a frame
-#[hawktracer(deblock_filter_frame)]
 pub fn deblock_filter_frame<T: Pixel>(
   deblock: &DeblockState, tile: &mut TileMut<T>, blocks: &TileBlocks,
   crop_w: usize, crop_h: usize, bd: usize, planes: usize,
@@ -1641,7 +1638,6 @@ fn sse_optimize<T: Pixel>(
   level
 }
 
-#[hawktracer(deblock_filter_optimize)]
 pub fn deblock_filter_optimize<T: Pixel, U: Pixel>(
   fi: &FrameInvariants<T>, rec: &Tile<U>, input: &Tile<U>,
   blocks: &TileBlocks, crop_w: usize, crop_h: usize,
diff --git a/src/encoder.rs b/src/encoder.rs
index c0b31fa..5fc753f 100644
--- a/src/encoder.rs
+++ b/src/encoder.rs
@@ -48,7 +48,6 @@ use std::sync::Arc;
 use std::{fmt, io, mem};
 
 use crate::rayon::iter::*;
-use rust_hawktracer::*;
 
 #[allow(dead_code)]
 #[derive(Debug, Clone, PartialEq)]
@@ -2949,7 +2948,6 @@ fn get_initial_cdfcontext<T: Pixel>(fi: &FrameInvariants<T>) -> CDFContext {
   cdf.unwrap_or_else(|| CDFContext::new(fi.base_q_idx))
 }
 
-#[hawktracer(encode_tile_group)]
 fn encode_tile_group<T: Pixel>(
   fi: &FrameInvariants<T>, fs: &mut FrameState<T>, inter_cfg: &InterConfig,
 ) -> Vec<u8> {
@@ -3181,7 +3179,6 @@ fn check_lf_queue<T: Pixel>(
   }
 }
 
-#[hawktracer(encode_tile)]
 fn encode_tile<'a, T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   fc: &'a mut CDFContext, blocks: &'a mut TileBlocksMut<'a>,
diff --git a/src/lrf.rs b/src/lrf.rs
index 2fc3b33..4399c4c 100644
--- a/src/lrf.rs
+++ b/src/lrf.rs
@@ -23,7 +23,6 @@ use crate::frame::{
 };
 use crate::tiling::{Area, PlaneRegion, PlaneRegionMut, Rect};
 use crate::util::{clamp, CastFromPrimitive, ILog, Pixel};
-use rust_hawktracer::*;
 
 use crate::api::SGRComplexityLevel;
 use std::cmp;
@@ -1461,7 +1460,6 @@ impl RestorationState {
     }
   }
 
-  #[hawktracer(lrf_filter_frame)]
   pub fn lrf_filter_frame<T: Pixel>(
     &mut self, out: &mut Frame<T>, pre_cdef: &Frame<T>,
     fi: &FrameInvariants<T>,
diff --git a/src/me.rs b/src/me.rs
index 6f849a4..7ed64e4 100644
--- a/src/me.rs
+++ b/src/me.rs
@@ -29,8 +29,6 @@ use crate::util::ILog;
 use std::ops::{Index, IndexMut};
 use std::sync::Arc;
 
-use rust_hawktracer::*;
-
 #[derive(Debug, Copy, Clone, Default)]
 pub struct MEStats {
   pub mv: MotionVector,
@@ -98,7 +96,6 @@ enum MVSamplingMode {
   CORNER { right: bool, bottom: bool },
 }
 
-#[hawktracer(estimate_tile_motion)]
 pub fn estimate_tile_motion<T: Pixel>(
   fi: &FrameInvariants<T>, ts: &mut TileStateMut<'_, T>,
   inter_cfg: &InterConfig,
diff --git a/src/scenechange/mod.rs b/src/scenechange/mod.rs
index 20479f0..126329d 100644
--- a/src/scenechange/mod.rs
+++ b/src/scenechange/mod.rs
@@ -14,7 +14,6 @@ use crate::encoder::Sequence;
 use crate::frame::*;
 use crate::sad_row;
 use crate::util::Pixel;
-use rust_hawktracer::*;
 use std::sync::Arc;
 use std::{cmp, u64};
 
@@ -123,7 +122,6 @@ impl<T: Pixel> SceneChangeDetector<T> {
   /// to the second frame in `frame_set`.
   ///
   /// This will gracefully handle the first frame in the video as well.
-  #[hawktracer(analyze_next_frame)]
   pub fn analyze_next_frame(
     &mut self, frame_set: &[Arc<Frame<T>>], input_frameno: u64,
     previous_keyframe: u64,
@@ -345,7 +343,6 @@ impl<T: Pixel> SceneChangeDetector<T> {
 
   /// The fast algorithm detects fast cuts using a raw difference
   /// in pixel values between the scaled frames.
-  #[hawktracer(fast_scenecut)]
   fn fast_scenecut(
     &mut self, frame1: Arc<Frame<T>>, frame2: Arc<Frame<T>>,
   ) -> ScenecutResult {
@@ -424,7 +421,6 @@ impl<T: Pixel> SceneChangeDetector<T> {
   /// We gather both intra and inter costs for the frames,
   /// as well as an importance-block-based difference,
   /// and use all three metrics.
-  #[hawktracer(cost_scenecut)]
   fn cost_scenecut(
     &self, frame1: Arc<Frame<T>>, frame2: Arc<Frame<T>>,
   ) -> ScenecutResult {
@@ -487,7 +483,6 @@ impl<T: Pixel> SceneChangeDetector<T> {
   }
 
   /// Calculates the average sum of absolute difference (SAD) per pixel between 2 planes
-  #[hawktracer(delta_in_planes)]
   fn delta_in_planes(&self, plane1: &Plane<T>, plane2: &Plane<T>) -> f64 {
     let mut delta = 0;
 
